/**
	@Author:	Jeroen Mathon
	@Description:	Dictionary class to hold the dictionary and to handle the translation
**/
#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <iostream>
#include <map>

using namespace std;

class Dictionary {
	public:
		Dictionary();		// Constructor
		~Dictionary() {};	// Deconstructor

		string translate(char input);

	private:
		map<char,string> dict;

};

#endif
