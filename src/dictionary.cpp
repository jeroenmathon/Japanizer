#include "dictionary.h"

Dictionary::Dictionary() {
	/* Populate dictionary */
	dict['a']="ka";	dict['b']="tu";
	dict['c']="mi";	dict['d']="te";
	dict['e']="ku";	dict['f']="lu";
	dict['g']="ji";	dict['h']="ri";
	dict['i']="ki";	dict['j']="zu";
	dict['k']="me";	dict['l']="ta";
	dict['m']="rin";dict['n']="to";	
	dict['o']="mo";	dict['p']="no";	
	dict['q']="ke";	dict['r']="shi";
	dict['s']="ari";dict['t']="chi";
        dict['u']="do";	dict['v']="ru"; 
        dict['w']="mei";dict['x']="na";	
        dict['y']="fu";	dict['z']="zi";	
}

string Dictionary::translate(char input) {
	return dict.at(input);
}
