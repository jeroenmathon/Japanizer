#include <stdio.h>
#include <string.h>
#include "dictionary.h"

using namespace std;

int main(int argc, char **argv)
{
	string input;

	cout << "Translate: ";
	getline(cin, input);
	cout << "\n";

	Dictionary dict;
	for(int i(0); i < input.length();i++) {
		if(input.c_str()[i] == ' ') {
			cout << " ";
		} else {
			if(isupper(input.c_str()[i])) {
			 	string out = dict.translate(tolower(input.c_str()[i]));
				out[0] = toupper(out[0]);
				cout <<	out;
			} else {
				cout <<	dict.translate(tolower(input.c_str()[i]));
			}
		}
	}
	cout << "\n";
	return 0;
}

